<div align="center">
  <img src="https://codeberg.org/teaserbot-labs/delightful-small-ai/raw/branch/main/small-ai-logo.png">
</div>

# delightful small artificial intelligence [![delightful](https://codeberg.org/teaserbot-labs/delightful/media/branch/main/assets/delightful-badge.png)](https://codeberg.org/teaserbot-labs/delightful)

A curated list of humane artificial intelligence resources that are open and accessible.

## Contents

- [Introduction](#introduction)
- [Maintainers](#maintainers)
- [Contributors](#contributors)
- [License](#license)

## Introduction

What is Small Al? Small Al is, of course, the little brother of Big Al. But jokes aside. Small Al is all what Big AI is not, namely small. 

> **Small Al** is all the good of AI without the bad. It is AI at 'human scale' and as such it is based on the principles of [Small Technology](https://small-tech.org/about/#small-technology) developed by [Laura Kalbag](https://laurakalbag.com) and [Aral Balkan](https://ar.al) at the [Small Technology Foundation](https://small-tech.org). It is the opposite of Big AI.

To learn more, click on any of the collapsed regions below.

<details><summary><b>AI for People</b></summary>
<br>

### AI for People

In summary, Small Al is intelligent systems technology for people. Small Al is for developers who want to leverage the power of the Semantic Web without having to learn abstract hierarchies. Small Al is technology for consumers who want or need more supportive technologies, that can take over some of the difficulties modern technologies and faster tech life cycles pose without volunteering increasingly more invasive intrusions into their privacy.

Small Al is all the good of AI without the bad. It is AI at 'human scale' and as such it is based on the principles of [Small Technology](https://small-tech.org) developed by Aral Balkan: _"Small Technology are everyday tools for everyday people designed to increase human welfare, not corporate profits."_

</details>

<details><summary><b>Big Al and Data Points</b></summary>
<br>

### Big Al and Data Points

Machine Learning (ML) gets better and more versatile only with exponentially increasingly larger sets of data points, making the approach fail to deliver in terms of generalizability. If it is trained to do X, then it can do X. And hopefully it is trained on really all potential types of Xs. For non-trained cases, like, e.g., a strange truck with supersized wheels it has never seen, no guarantee can be made. Will it be classified as a vehicle or as a road sign by your autonomous vehicle? You will see, when it tries to pass underneath it or not. In the former case, you will be delighted spreading the news how great your autonomous car is. In the latter case, you may not have the chance anymore to give your opinion.

A major open ML vendor is not making their system publicly available [but just via an API](https://fossandcrafts.org/episodes/2-machine-learning-impact.html): "many of the models underlying the API are very large, taking a lot of expertise to develop and deploy, and making them very expensive to run. This makes it hard for anyone except larger companies to benefit from the underlying technology." Some may suspect that they just want to keep the Intellectual Property to themselves diverging from the principles of openness, which they might, but this is also just the nature of this technology. In addition the sheer amount of data collection poses serious risks to our privacy, and compounds the problem of [surveillance capitalism](https://en.wikipedia.org/wiki/Surveillance_capitalism).

Small Al is technology designed so it can do without large amounts of data points. This makes it not only more cost-effective but also more privacy-aware. Small Al does not need constant streams of data from cameras and microphones to be sent to a data center to get better.

</details>

<details><summary><b>Small Al and Ontologies</b></summary>
<br>

### Small Al and Ontologies

Semantic Web (SemWeb) or Knowledge Representation (KR) gets better by adding more concept definitions. While it does not have issues with privacy and storage, developers suffer from this growth. While, e.g., Web Ontology Language (OWL) and other ontology representations are human-readable, growth of knowledge bases hits both conceptual boundaries with developers and computational boundaries, as complex concepts - if designed naively by a developer - can have considerable impact on performance. 

Data points are not the issue with KR, everything can be designed and tested and proven to work in the lab. It can even be made to undergo stress testing of expensive boundary cases. In contrast to ML, KR is not black box. It can be mathematically checked to work like a network protocol. High risk applications are thus safer with a KR implementation, but it may lack in understandability. As with a network protocol, we need a thorough understanding of the formal specification, which can be an enormous burden for developers. 

An experienced developer [summarizes](https://forum.solidproject.org/t/constructive-criticism-from-an-experienced-developer/3521) the situation: _"As a newcomer to the Semantic Web aspects of this, it’s a bit daunting to see entities defined with 100+ fields (https://schema.org/Person), often each field with its own complex sub-schemas, with mind-bendingly abstract hierarchies, links to 20 other 50-page specs, defining what 'is' is, what a Thing is, what the definition of a definition is, etc. In the absence of artificial general intelligence, human programmers will have to end up reading this stuff anyway"_. Some of the practical developer issues with SemWeb are described in more detail by Kevin Feeney in a four-part series on the TerminusDB blog, see e.g. ["Graph Fundamentals — Part 4: Linked Data"](https://terminusdb.com/blog/2020/02/25/graph-fundamentals-part-4-linked-data/).

Small Al is technology designed so it can do without large libraries of concepts. This makes developing with it not only more cost-effective and faster, but also more fun and easier for developers. Concepts are defined not so as to primarily respect and reflect philosophical traditions, but with respect to usability for concrete everyday reasoning tasks developers need. 

</details>

<br>

## Maintainers

If you have questions or feedback regarding this list, then please create an [Issue](https://codeberg.org/teaserbot-labs/delightful-gamification/issues) in our tracker, and optionally `@mention` one or more of our maintainers:

- [`@circlebuilder`](https://codeberg.org/circlebuilder)
- [`@schmidtke`](https://codeberg.org/schmidtke)

## Contributors

With delight we present you some of our [delightful contributors](delightful-contributors.md) (please [add yourself](https://codeberg.org/teaserbot-labs/delightful/src/branch/main/delight-us.md#attribution-of-contributors) if you are missing).

## License

[![CC0 Public domain. This work is free of known copyright restrictions.](https://i.creativecommons.org/p/mark/1.0/88x31.png)](LICENSE)

